
export default function (s) {
  let tokens = []

  let mode = 'WORD'
  let next = mode
  let start = 0
  let end = 0

  let unescape = () =>
    s
      .substring(start, end)
      .replace(/\\\{/g, '{').replace(/\\\\/g, '\\')
      .replace(/\\\}/g, '}')

  let pushWord = () => tokens.push(unescape())
  let pushParam = () => tokens.push({ param: unescape() })

  for (; end < s.length; end++) {
    let c = s[end]
    switch (mode) {
      case 'ESCAPE':
        switch (c) {
          case '\\':
          case '{':
          case '}':
            mode = next
            break
          default:
            throw new Error('incomplete escape')
        }
        break
      case 'WORD':
        switch (c) {
          case '\\':
            next = mode
            mode = 'ESCAPE'
            break
          case '{':
            if (start < end) {
              pushWord()
            }
            mode = 'PARAM'
            start = end + 1
            break
          case '}':
            throw new Error('unopened parameter')
        }
        break
      case 'PARAM':
        switch (c) {
          case '\\':
            next = mode
            mode = 'ESCAPE'
            break
          case '{':
            throw new Error()
          case '}':
            pushParam()
            mode = 'WORD'
            start = end + 1
            break
        }
        break
      default:
        throw new Error('unknown mode')
    }
  }
  switch (mode) {
    case 'WORD':
      if (end > start) {
        pushWord()
      }
      break
    case 'PARAM':
      throw new Error('unclosed parameter')
    case 'ESCAPE':
      throw new Error('incomplete escape')
    default:
      throw new Error('unknown mode')
  }

  return params => tokens.map(function (token) {
    if (typeof token === 'string') {
      return token
    } else {
      let param = token.param
      if (params.hasOwnProperty(param)) {
        let v = params[param]
        if (typeof v !== 'string') {
          throw new Error(`parameter "${param}" must be of type string`)
        }
        return v
      } else {
        throw new Error(`missing required template parameter "${param}"`)
      }
    }
  }).join('')
}
