/* eslint-env jest */

import template from '../src/index'

let testCases = [
  {
    description: 'simple example',
    input: {
      template: '/name/{name}',
      params: { name: 'andrew' }
    },
    output: '/name/andrew'
  },
  {
    description: 'simple example with duplicate',
    input: {
      template: '/name/{name}{name}',
      params: { name: 'andrew' }
    },
    output: '/name/andrewandrew'
  },
  {
    description: 'minimal example',
    input: {
      template: '{name}',
      params: { name: '' }
    },
    output: ''
  },
  {
    description: 'minimal example',
    input: {
      template: '{name}',
      params: { name: 'andrew' }
    },
    output: 'andrew'
  },
  {
    description: 'minimal example',
    input: {
      template: 'no-substitution-to-see-here',
      params: {}
    },
    output: 'no-substitution-to-see-here'
  },
  {
    description: 'escape open character',
    input: {
      template: 'no-substitution-to-see-here-\\{-escaped-open',
      params: {}
    },
    output: 'no-substitution-to-see-here-{-escaped-open'
  },
  {
    description: 'escape open character',
    input: {
      template: 'greeting={we\\{ird}',
      params: {
        'we{ird': 'hi-there'
      }
    },
    output: 'greeting=hi-there'
  }
]

for (let { description, input, output } of testCases) {
  test(description, () => {
    expect(template(input.template)(input.params))
      .toBe(output)
  })
}

let errorCases = [
  {
    input: {
      template: '{name}',
      params: {}
    },
    error: 'missing required template parameter "name"'
  },
  {
    input: {
      template: '{name',
      params: {}
    },
    error: 'unclosed parameter'
  },
  {
    input: {
      template: '{name',
      params: {}
    },
    error: 'unclosed parameter'
  },
  {
    input: {
      template: 'na{me',
      params: {}
    },
    error: 'unclosed parameter'
  },
  {
    input: {
      template: 'name{',
      params: {}
    },
    error: 'unclosed parameter'
  },
  {
    input: {
      template: '}name',
      params: {}
    },
    error: 'unopened parameter'
  },
  {
    input: {
      template: 'n}ame',
      params: {}
    },
    error: 'unopened parameter'
  },
  {
    input: {
      template: 'na}me',
      params: {}
    },
    error: 'unopened parameter'
  },
  {
    input: {
      template: 'nam}e',
      params: {}
    },
    error: 'unopened parameter'
  },
  {
    input: {
      template: 'name}',
      params: {}
    },
    error: 'unopened parameter'
  }
]

for (let { input, error } of errorCases) {
  test(error, () => {
    expect(() => template(input.template)(input.params))
      .toThrow(error)
  })
}
