
## Install ##
```
yarn install super-simple-string-template
```

## Usage ##

```javascript
import template from 'super-simple-string-template'
template('/home/{user}')({ user: 'andrew' })
```
